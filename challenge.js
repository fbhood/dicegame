/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

// create fundamentals variables for the game.
var scores, roundScore, activePlayer, gamePlaying;
init();
var lastDice;
var lastSecondDice;
var winningScore;
document.querySelector(".btn-roll").addEventListener("click", function() {
  if (gamePlaying) {
    // 1. need a random number
    var dice = Math.floor(Math.random() * 6) + 1;

    // 2. display the results
    var diceDOM = document.querySelector(".dice");
    diceDOM.style.display = "block";
    diceDOM.src = "dice-" + dice + ".png";

    // Display a second dice
    var secondDice = Math.floor(Math.random() * 6) + 1;

    var diceDOM = document.querySelector(".second-dice");
    diceDOM.style.display = "block";
    diceDOM.src = "dice-" + secondDice + ".png";

    var input = document.getElementById("set-points").value;
    if (input) {
      winningScore = input;
    } else {
      winningScore = 20;
    }
    //3. upate the round score if the rolled number was not a 1

    if (
      (dice === 6 || secondDice === 6) &&
      (lastDice === 6 || lastSecondDice === 6)
    ) {
      //console.log(dice, lastDice, secondDice, lastSecondDice);
      alert(
        `The last two dice were: ${(lastDice, lastSecondDice)}
        Current Dices are: ${(dice, secondDice)}, 
        You got two six in a row! :( you loose all your score!`
      );

      scores[activePlayer] = 0;
      roundScore = 0;
      document.querySelector(
        "#current-" + activePlayer
      ).textContent = roundScore;
      document.querySelector("#score-" + activePlayer).textContent =
        scores[activePlayer];

      nextPlayer();
    } else if (dice !== 1 && secondDice !== 1) {
      //console.log(dice, secondDice, lastDice, lastSecondDice);

      // add score
      roundScore += dice + secondDice;
      //console.log(roundScore);
      document.querySelector(
        "#current-" + activePlayer
      ).textContent = roundScore;
    } else {
      //console.log(dice, secondDice, lastDice, lastSecondDice);
      alert(
        `First Dice: ${dice}, Second Dice: ${secondDice}, ! Hey, you got a one! :( it's the next player turn now!`
      );
      // next player
      nextPlayer();
    }
    lastDice = dice;
    lastSecondDice = secondDice;
  }
});

function nextPlayer() {
  activePlayer === 0 ? (activePlayer = 1) : (activePlayer = 0);
  roundScore = 0;
  document.getElementById("current-0").textContent = 0;
  document.getElementById("current-1").textContent = 0;

  document.querySelector(".player-0-panel").classList.toggle("active");
  document.querySelector(".player-1-panel").classList.toggle("active");

  document.querySelector(".dice").style.display = "none";
  document.querySelector(".second-dice").style.display = "none";
}

document.querySelector(".btn-hold").addEventListener("click", function() {
  if (gamePlaying) {
    // add current score to the player global scope
    scores[activePlayer] += roundScore;

    // update the UI
    document.querySelector("#score-" + activePlayer).textContent =
      scores[activePlayer];

    // Check if player won the game (when the global score is 100 the player won)
    if (scores[activePlayer] >= winningScore) {
      document.getElementById("name-" + activePlayer).textContent = "Winner";
      document.querySelector(".dice").style.display = "none";
      document.querySelector(".second-dice").style.display = "none";

      document
        .querySelector(".player-" + activePlayer + "-panel")
        .classList.add("winner");
      document
        .querySelector(".player-" + activePlayer + "-panel")
        .classList.remove("active");
      gamePlaying = false;
      document.querySelector(".btn-roll").style.display = "none";
      document.querySelector(".btn-hold").style.display = "none";
    } else {
      // next player
      nextPlayer();
    }
  }
});

document.querySelector(".btn-new").addEventListener("click", init); // call the function as paramenter to the event listener

function init() {
  scores = [0, 0];
  roundScore = 0;
  activePlayer = 0;
  gamePlaying = true;
  document.querySelector(".dice").style.display = "none";
  document.querySelector(".second-dice").style.display = "none";
  document.getElementById("score-0").textContent = "0";
  document.getElementById("score-1").textContent = "0";
  document.getElementById("current-0").textContent = "0";
  document.getElementById("current-1").textContent = "0";
  document.getElementById("name-0").textContent = "Player 1";
  document.getElementById("name-1").textContent = "Player 2";
  document.querySelector(".player-0-panel").classList.remove("winner");
  document.querySelector(".player-1-panel").classList.remove("winner");
  document.querySelector(".player-0-panel").classList.remove("active");
  document.querySelector(".player-0-panel").classList.add("active");
  document.querySelector(".player-1-panel").classList.remove("active");
  document.querySelector(".btn-roll").style.display = "block";
  document.querySelector(".btn-hold").style.display = "block";
}

/* 3 Challenges 

TODO: Change the game to the following rules:

1. A player looses his entire score when he rolls two six in a row. After that, it's the next player turn.
Hint: Always save the previous dice roll in a separate variable. 


2. Add an input field to the HTML where players can set the winning score, so that they can change the predefined
score of 100. 
Hint: you can read the value with the value property in JavaScript. Use google to figure it out.

3. Add another dice to the game, so that there are two dices now. The plater looses his current score when one of them is a 1.
Hint: you will need CSS to position the second dice, so take a look at the css code for the first one.

4. Optional - Make the page responsive.

*/
